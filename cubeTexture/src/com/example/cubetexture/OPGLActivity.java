package com.example.cubetexture;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.WindowManager;

public class OPGLActivity extends Activity {

	private GLSurfaceView glView;   
	  
	   
	   @Override
	   protected void onCreate(Bundle savedInstanceState) {
	      super.onCreate(savedInstanceState);
	      getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	      glView = new OPGLSurfaceView(this);           
	      this.setContentView(glView);                // ustawiamy kontekst rysowania
	   }
	   
	   
	   @Override
	   protected void onPause() {
	      super.onPause();
	      glView.onPause();
	   }
	   
	  
	   @Override
	   protected void onResume() {
	      super.onResume();
	      glView.onResume();
	   }

}
