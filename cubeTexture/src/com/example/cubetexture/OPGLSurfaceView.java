package com.example.cubetexture;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

public class OPGLSurfaceView extends GLSurfaceView {
	OPGLRenderer renderer;
	
	private final float multiply = 100000.0f;
	   private float previousX;
	   private float previousY;
	   
	   
	public OPGLSurfaceView(Context context) {
		super(context);
		renderer = new OPGLRenderer(context);
	      this.setRenderer(renderer);
	      this.requestFocus();  
	      this.setFocusableInTouchMode(true);
	}
	public boolean onTouchEvent(final MotionEvent evt) {
	      float currentX = evt.getX();
	      float currentY = evt.getY();
	      float deltaX, deltaY;
	      
	      switch (evt.getAction()) {
	         case MotionEvent.ACTION_MOVE:
	            	        	 
	            deltaX = currentX - previousX;
	            deltaY = currentY - previousY;
	            renderer.angleX += deltaY * multiply;
	            renderer.angleY += deltaX * multiply;
	      }
	      
	      previousX = currentX;
	      previousY = currentY;
	      return true;  
	   }

}
