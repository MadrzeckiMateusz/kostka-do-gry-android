package com.example.cubetexture;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

public class Cube3dTexture {
	  private FloatBuffer vertexBuffer; // bufory danych
	  private FloatBuffer texBuffer;
	  
	  private int numFaces = 6;
	  private float[] vertices = { // wierzcho�ki dla z 0 
	      -1.0f, -1.0f, 0.0f,  // 0. lewy dolny
	       1.0f, -1.0f, 0.0f,  // 1. prawy dolny
	      -1.0f,  1.0f, 0.0f,  // 2. lewy gorny
	       1.0f,  1.0f, 0.0f   // 3. prawy gorny
	   };
	  
	   float[] texCoords = { // koordynaty textur
			      0.0f, 1.0f,  
			      1.0f, 1.0f,  
			      0.0f, 0.0f, 
			      1.0f, 0.0f   
			   };
	   private int[] imageFilesIDs = {// numery id textur
			   R.drawable.bok1,
			   R.drawable.bok2,
			   R.drawable.bok3,
			   R.drawable.bok4,
			   R.drawable.bok5,
			   R.drawable.bok6
	   };
	   
	   private int[] textureIDs = new int[numFaces];  // tablica przekonwertowanych textur
	   private Bitmap[] bitmap = new Bitmap[numFaces]; // tablica wczytanych bitmap
	   

	
	   
	   public Cube3dTexture() {
		   ByteBuffer vbb = ByteBuffer.allocateDirect(12 * 4 );
		      vbb.order(ByteOrder.nativeOrder());
		      vertexBuffer = vbb.asFloatBuffer();
		      vertexBuffer.put(vertices);
		      vertexBuffer.position(0);    
		  
		      ByteBuffer tbb = ByteBuffer.allocateDirect(texCoords.length * 4 );
		      tbb.order(ByteOrder.nativeOrder());
		      texBuffer = tbb.asFloatBuffer();
		      texBuffer.put(texCoords);
		      texBuffer.position(0);   
		   }
	   
	   
	   
	  
	   public void draw(GL10 gl) {
	      gl.glFrontFace(GL10.GL_CCW);    // przednia sciana rysowana zgodnie ze wskaz�wkami zegara
	      
	      gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);// w��czamy tablice wierzcho�k�w
	      gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);// w�. tabice koordynat�w dla tekstury
	      gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
	      gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, texBuffer);// definiowanie tablic
	      
	      // front
	      gl.glPushMatrix();//wrzucenie na stos przed transformacja
	      gl.glTranslatef(0.0f, 0.0f, 1.0f);//przemieszczenie obiektu w osi z
	      gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[0]);// przypisanie tekstury
	      gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);//rysowanie tablicy 
	      gl.glPopMatrix();
	  
	      // left
	      gl.glPushMatrix();
	      gl.glRotatef(270.0f, 0f, 1f, 0f);
	      gl.glTranslatef(0f, 0f, 1.0f);
	      gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[1]);
	      gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
	     gl.glPopMatrix();
	  
	      // back
	      gl.glPushMatrix();
	      gl.glRotatef(180.0f, 0f, 1f, 0f);
	      gl.glTranslatef(0f, 0f, 1.0f);
	      gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[5]);
	      gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
	      gl.glPopMatrix();
	      // right
	      gl.glPushMatrix();
	      gl.glRotatef(90.0f, 0f, 1f, 0f);
	      gl.glTranslatef(0f, 0f, 1.0f);
	      gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[4]);
	      gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
	      gl.glPopMatrix();
	 
	      // top
	      gl.glPushMatrix();
	      gl.glRotatef(270.0f, 1f, 0f, 0f);
	      gl.glTranslatef(0f, 0f, 1.0f);
	      gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[2]);
	      gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
	      gl.glPopMatrix();
	 
	      // bottom
	      gl.glPushMatrix();
	      gl.glRotatef(90.0f, 1f, 0f, 0f);
	      gl.glTranslatef(0f, 0f, 1.0f);
	      gl.glBindTexture(GL10.GL_TEXTURE_2D, textureIDs[3]);
	      gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
	      gl.glPopMatrix();
	  
	      gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
	      gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);// wy�aczenie tablic 
	     
	   }
	   public void loadTexture(GL10 gl, Context context ){
		   gl.glGenTextures(6, this.textureIDs, 0);// generowanie identyfikator�w textur
		  
		   for (int face = 0; face < numFaces; face++) {//�adowanie bit mapy
			   bitmap[face] = BitmapFactory.decodeStream(
		               context.getResources().openRawResource(imageFilesIDs[face]));
		         gl.glBindTexture(GL10.GL_TEXTURE_2D, this.textureIDs[face]);
		         
		         GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, this.bitmap[face], 0);
		         gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER,GL10.GL_NEAREST); // filtry tekstury
		         this.bitmap[face].recycle();//czyszczenie
		      }
}
}
