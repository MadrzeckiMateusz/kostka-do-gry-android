package com.example.cubetexture;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;


public class MenuActivity extends Activity implements OnClickListener{

	
	private Intent intent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);// pe�en ekran
		
		Button btn1 = (Button) this.findViewById(R.id.button1);
		btn1.setOnClickListener(this);
		
		intent = new Intent(getApplicationContext(), OPGLActivity.class);// intencja wywo�ujaca aktownos� rysowania kostki
	}

	@Override
	public void onClick(View v) {
		
		startActivity(intent);
	}

	
	

}
