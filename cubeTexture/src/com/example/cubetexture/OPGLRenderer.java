package com.example.cubetexture;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLU;

public class OPGLRenderer implements Renderer {
	    
	
	   private Cube3dTexture cube3D;;

	   float angleX=0.0f ;
	   float angleY=0.0f ;
	 
	   private Context context;
	   
	   public OPGLRenderer(Context context) {
	      this.context = context;
		   cube3D = new Cube3dTexture();
	   }
	   
	  
	   @Override
	   public void onSurfaceCreated(GL10 gl, EGLConfig config) {
	      gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  // kolor t�a na czarne

	      gl.glEnable(GL10.GL_DEPTH_TEST);   // w��cznie bufora do usuwania niewidzialnych powierzchni
	      gl.glDepthFunc(GL10.GL_LEQUAL);    // Tfunkcja do testowania g�ebokosci
	   	     
	      cube3D.loadTexture(gl,context);
	      gl.glEnable(GL10.GL_TEXTURE_2D);
	      
	     
	   }
	   
	   
	   public void onSurfaceChanged(GL10 gl, int width, int height) {
	    
	      float aspect = (float)width / height;
	   	     
	      gl.glViewport(0, 0, width, height); // ustawienie widoku
	      gl.glMatrixMode(GL10.GL_PROJECTION); // w��cznie matrixa projekcji
	      gl.glLoadIdentity();                 // Reset projection matrix  
	      GLU.gluPerspective(gl, 45, aspect, 0.1f, 100.f);
	         
	   }
	   
	   
	   public void onDrawFrame(GL10 gl) {
	     		   
	      gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
	      gl.glMatrixMode(GL10.GL_MODELVIEW);
	      gl.glLoadIdentity(); 
	      GLU.gluLookAt(gl, -6, 5, 5, 0f, 0f, 0.0f, 0.0f, 1.0f, 0.5f); //ustawienia kamery
	          	   	     	
	      gl.glRotatef(angleX, 0.0f, 0.5f, 0.0f);
	      gl.glRotatef(angleY, 0.5f, 0.5f, 0.0f);//rotacja
	      
	      cube3D.draw(gl);
	     
	     
	      
	   }
}
